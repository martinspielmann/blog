This blog gathers `anti patterns`, `bugs`, `vulnerabilities`, `bad practices`, `misconfigurations`, so you can learn how to do it right by learning how to not do it wrong.

If you want others to be able to learn from your bad example, just open an issue or PR on [codeberg.org](https://codeberg.org/pingunaut/bad-example).