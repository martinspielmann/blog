---
title: Docker and UFW
subtitle: How to mess up your firewall in three easy steps
date: 2020-02-21
tags: ["misconfiguration", "docker", "ufw"]
draft: false

---

The way Docker manipulates `iptables` actually bypasses `ufw`, which is the default firewall on Ubuntu systems.
You can block a port, spin up a container and in a blink of an eye the blocked port will be open again. It will even bypass Any/Any/Deny rules.
To make the magic happen just follow the steps below.

### Details

Lets see how to reproduce this in detail with the example of a Tomcat container.

1. On your host system use `ufw` to deny access to a specific port
```bash
    sudo ufw deny 8080
    # Or if you want to block all incoming connections by default, use the command below but make sure you do not lock you out.
    # sudo ufw default deny incoming
```
2. Check if your rule has been added
```bash
    sudo ufw status verbose
```
3. Forward a Docker port to the aforementioned port
```bash
    docker run -it --rm -p 8080:8080 tomcat:8.0
```
4. Try to access Tomcat from another computer. Tadaa! The port is now accessible from the outside although `ufw status` will show you that it's not. You will be welcomed by your favorite Tomcat status page.
```bash
    curl [your-computer-name-running-docker]:8080
```

### References

Discussions about the problem and possible solutions can be found in the sources below. My suggestion after spending a lot of time on this topic: If you are not forced to stick to `ufw`, just use iptables directly or another frontend like `firewalld` which works with docker like a charm out of the box.
- https://github.com/chaifeng/ufw-docker
- https://stackoverflow.com/questions/30383845/what-is-the-best-practice-of-docker-ufw-under-ubuntu