---
title: "First Post"
date: 2020-02-20T12:50:48+01:00
author: "Martin Spielmann"
type: ""
subtitle: "bad-example.com will gather anti patterns, bugs, vulnerabilities, bad practices, misconfigurations"
image: ""
tags: ["meta"]
---

This is my first post, how exciting!

I will try to dig deeper into everything that went wrong during the days of working describe it here, so you do not have to figure out the same pitfalls again. I'd love if you join me and send me your daily bad examples. You can do so via any channel you like, the preferred way is to create an issue or PR on [codeberg.org](https://codeberg.org/pingunaut/bad-example).

Besides adhoc posts, there will be recurring categories published on a regular basis.
A first regular category is the [stackoverflow-crappy-answer](/post/stackoverflow-crappy-answer/). I search for the most down-voted answers on StackOverflow and we'll see what's wrong with them.

Have fun!