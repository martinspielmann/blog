---
title: About
subtitle: Why you'd want to waste your time on this site
comments: false
---

There are countless resources online guiding and teaching you how to design and implement things right. 
The idea of this blog is doing it the other way round: provide information to not do things wrong.
